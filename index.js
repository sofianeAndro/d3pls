let $ = require('jquery')
var d3 = require("d3");

 exports.printchart=function(data){
    d3.select(".chart")
        .selectAll("div")
        .data(data)
        .enter().append("div")
        .style("width", function(d) { return d * 50 +"px"; })
        .text(function(d) { return d; });
}
